pathmenu module

The pathmenu module allows you to define rules thus defining more precisely
the way Drupal should organize the menu and breadcrumb.

------------------------------------------------------------------------------

1 Installation

The pathmenu module follows the traditional Drupal way for installing module.
It does not create or alter any table of the database.

Access control is done via the 'administer pathmenu' access right.

------------------------------------------------------------------------------

2 Pathmenu language

The pathmenu language is a very simple language where each element is written
on one line.

There are 2 types of element :
    * rule,
    * comment.

The rules are checked in the writing order. If a rule can be executed,
execution is stopped at this point, the remaining rules are ignored.

Blank lines are ignored and can be used to separate rules.

2.1 Rule

A rule is composed of condition(s) and action(s) separated by the '->'
operator.
A rule must be written on a unique line.
A rule looks like :
cond(parm)+cond(parm)+cond(parm)->action(parm)+action(parm)+action(parm)

2.1.1 Conditions

A condition has always the same form : condition(parameter)

Parameter should not have single nor double quotes enclosing it, it should be
written as is.

Conditions can be ANDed with the '+' operator.

   1. nodetype
      To be true :
          * page must be a node,
          * the node type must be the one specified as parameter.

   2. nodeid
      To be true :
          * page must be a node,
          * the node ID must be the one specified as parameter.

   3. term
      To be true :
          * page must be a node,
          * the node must be associated with the term whose term ID is
            specified as parameter.

   4. urlstart
      To be true, the current URI must start with the string specified as
      parameter. For those without URL rewriting, the "/?q=" usual starting
      string is removed before comparison.

   5. status
      To be true :
          * page must be a node
          * the node status must match the one specified as the parameter.

      The allowed values of parameter are :
          * published
          * unpublished
          * promoted
          * notpromoted
          * sticky
          * notsticky

   6. userrole
      To be true, the user must have the role specified as the parameter (the
      human readable role). It also works with the anonymous user.

   7. isnode
      To be true, page must be a node. Use 'this' as the parameter.

2.1.2 Actions

An action has always the same form : action(parameter)
Parameter should not have single nor double quotes enclosing it, it should be
written as is.

Actions can be sequenced with the '+' operator.

   1. parentmenu
      Dynamically attach the current page to the specified menu ID. Breadcrumb
      is also set according to the breadcrumb of the specified menu.

   2. parentpath
      Dynamically attach the current page to the specified path. Breadcrumb is
      also set according to the breadcrumb of the specified path.

   3. localtask
      Add a local task to the current page.

   4. goto
      Use the specified parameter with the drupal_goto function to move to a
      different location.

   5. menuadd
      Adds an arbitrary dynamic menu item. It takes 3 parameters :
          * the menu ID to which the new menu must be attached,
          * the title of the menu item to add,
          * the path to which it points.

   6. message
      Use drupal_set_message to display a message specified as parameter.

2.2 Comment

When a line starts with '//', it is considered a comment.

------------------------------------------------------------------------------

3 Hooks

You can add new conditions and new actions available by developping them
without modifying the pathmenu module. This is done with hook_pmcondition and
hook_pmaction.

3.1 hook_pmcondition

hook_pmcondition takes 3 parameters :
    * $op : a string containing the selected operation, either getlist or
      getcode
    * $name : a string containing the name of the condition, should contain
      only alphanumerical characters with no spaces (only meaningful when
      $op is getcode)
    * $parameter : a string containing the parameter (or parameters) passed
      with the condition (only meaningful when $op is getcode), it's up to the
      hook to handle the separation of parameters.

The getlist operation should return an associated array with the keys being
the conditions names available and the value being the help text appearing on
the admin page of the pathmenu module.

The getcode operation should return a string containing the PHP code of the
condition with the parameter. This must be PHP code that can be inserted in an
'if' instruction with other conditions sperated by 'and'.

3.2 hook_pmaction

hook_pmaction takes 3 parameters :
    * $op : a string containing the selected operation, either getlist or
      getcode
    * $name : a string containing the name of the action, should contain only
      alphanumerical characters with no spaces (only meaningful when $op is
      getcode)
    * $parameter : a string containing the parameter (or parameters) passed
      with the action (only meaningful when $op is getcode), it's up to the
      hook to handle the separation of parameters.

The getlist operation should return an associated array with the keys being
the actions names available and the value being the help text appearing on the
admin page of the pathmenu module.

The getcode operation should return a string containing the PHP code of the
action with the parameter. This must be PHP code statement ended with a
semi-colon and should be syntactically correct alone.

<?php and ?> must no be specified.

------------------------------------------------------------------------------

4 Samples

// If the node is associated with term whose tid is 30, attach it to the menu
// whose mid is 23
term(30)->parentmenu(23)

// Show a message if an admin user is seeing a story type node
userrole(admin user)+nodetype(story)->message(This is a story)

// If URI starts with news/ and the node type is story, show a message and
// attach the node to the menu whose mid is 24
urlstart(news/)+nodetype(story)->message(This is a news)+parentmenu(24)
