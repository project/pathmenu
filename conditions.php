<?php
function pathmenu_pmcondition($op='getlist',$name=NULL,$parameter=NULL) {
  $list=array(
    'nodetype'=>t("Page must be a node and the node type must be the one specified as parameter."),
    'nodeid'=>t("Page must be a node and the node ID must be the one specified as parameter."),
    'term'=>t("Page must be a node and the node must be associated with the term whose ID is specified as parameter."),
    'urlstart'=>t("URI of the current page must start by the parameter. There's no need to specify /?q=."),
    'status'=>t("Page must be node and the node must have the status specified as parameter. Available status are published, unpublished, promoted, notpromoted, sticky, notsticky"),
    'userrole'=>t("The current user must have the role specified as parameter. This is the human readable identified of the role."),
    'isnode'=>t("Page must be a node.")
  );

  switch($op) {
    case 'getlist':
      return $list;
    
    case 'getcode':
      if(!in_array($name,array_keys($list))) {
        return '';
      } else {
        $function='pathmenu_pmcondition_'.$name;
        return $function($parameter);
      }
  }
}

function pathmenu_pmcondition_nodetype($parameter) {
  $parameter=addcslashes($parameter,"'");
  return "\$nodetype=='".$parameter."'";
}

function pathmenu_pmcondition_nodeid($parameter) {
  $parameter=addcslashes($parameter,"'");
  return "\$nodenid=='".$parameter."'";
}

function pathmenu_pmcondition_term($parameter) {
  $parameter=addcslashes($parameter,"'");
  return "in_array('".$parameter."',\$nodeterms)";
}

function pathmenu_pmcondition_urlstart($parameter) {
  $paramslash=addcslashes($parameter,"'");
  return "substr(\$url,0,".strlen($parameter).")=='".$paramslash."'";
}

function pathmenu_pmcondition_status($parameter) {
  switch(strtolower($parameter)) {
    case 'published'  : return "\$node->status>=1";
    case 'unpublished': return "\$node->status<1";
    case 'promoted'   : return "\$node->promote>=1";
    case 'notpromoted': return "\$node->promote<1";
    case 'sticky'     : return "\$node->sticky>=1";
    case 'notsticky'  : return "\$node->sticky<1";
    default:
      drupal_set_message(
        t(
          "Unknown parameter [%parameter] for condition [%condition].",
          array(
            "%parameter"=>$parameter,
            "%condition"=>$condition['function']
          )
        ),
        'error'
      );
      return FALSE;
  }
}

function pathmenu_pmcondition_userrole($parameter) {
  $parameter=addcslashes($parameter,"'");
  return "in_array('".$parameter."',\$user->roles)";
}

function pathmenu_pmcondition_isnode($parameter) {
  return "\$nodetype!==''";
}
