<?php
function pathmenu_pmaction($op='getlist',$name=NULL,$parameter=NULL) {
  $list=array(
    'parentmenu'=>t("Dynamically attach the current page to the specified menu ID. Breadcrumbe is also set according to the breadcrumb of the specified menu."),
    'parentpath'=>t("Dynamically attach the current page to the specified path. Breadcrumb is also set according to the breadcrumb of the specified path."),
    'localtask'=>t("Add a local task to the current page."),
    'goto'=>t("Use the specified parameter with the drupal_goto function to move to a different location. Following actions will be ignored. Use &lt;front&gt; to go to front page."),
    'menuadd'=>t("Adds an arbitrary dynamic menu item. It takes 3 parameters : the menu ID to which the new menu item must be attached, the title of the menu item to add, the path to which it points."),
    'message'=>t("Use drupal_set_message to display a message specified as parameter.")
  );

  switch($op) {
    case 'getlist':
      return $list;
    
    case 'getcode':
      if(!in_array($name,array_keys($list))) {
        return '';
      } else {
        $function='pathmenu_pmaction_'.$name;
        return $function($parameter);
      }
  }
}

function pathmenu_pmaction_parentmenu($parameter) {
  $parameter=addcslashes($parameter,"'");
  return "menu_set_location(array(menu_get_item('".$parameter."'),array('path'=>'\$url','title'=>'Nothing','type'=>MENU_VISIBLE_IN_BREADCRUMB)));";
}

function pathmenu_pmaction_parentpath($parameter) {
  $parameter=addcslashes($parameter,"'");
  return "menu_set_location(array(menu_get_item(NULL,'".$parameter."'),array('path'=>'\$url','title'=>'Nothing','type'=>MENU_VISIBLE_IN_BREADCRUMB)));";
}

function pathmenu_pmaction_localtask($parameter) {
  $parameter=addcslashes($parameter,"'");
  $parameters=explode(',',$parameter);
  if(count($parameters)!=2) {
    drupal_set_message(t("[localtask] action must have 2 parameters"),'error');
    return FALSE;
  }

  $title=$parameters[0];
  $path =$parameters[1];
  $type =MENU_LOCAL_TASK;
  return "_pathmenu_add_menu(array('title'=>t(".$title."),'path'=>'".$path."','type'=>".$type.",'weight'=>5));";
}

function pathmenu_pmaction_goto($parameter) {
  $parameter=addcslashes($parameter,"'");
  return "drupal_goto('".$parameter."');";
}

function pathmenu_pmaction_menuadd($parameter) {
  $parameter=addcslashes($parameter,"'");
  $parameters=explode(',',$parameter);

  if(count($parameters)!=3) {
    drupal_set_message(t("[menuadd] action must have 3 parameters"),'error');
    return FALSE;
  }

  $pid  =$parameters[0];
  $title=$parameters[1];
  $path =$parameters[2];
  $type =MENU_DYNAMIC_ITEM;

  return "_pathmenu_add_menu(array('pid'=>'".$pid."','title'=>t(".$title."),'path'=>'".$path."','type'=>".$type."));";
}

function pathmenu_pmaction_message($parameter) {
  $parameter=addcslashes($parameter,"'");
  return "drupal_set_message(t('".$parameter."'));";
}
